"""
Your awesome Distance Vector router for CS 168
"""

import sim.api as api
import sim.basics as basics


# We define infinity as a distance of 16.
INFINITY = 16


class DVRouter (basics.DVRouterBase):
  # NO_LOG = True # Set to True on an instance to disable its logging
  POISON_MODE = True # Can override POISON_MODE here
  DEFAULT_TIMER_INTERVAL = 30 # Can override this yourself for testing

  def __init__ (self):
    """
    Called when the instance is initialized.

    You probably want to do some additional initialization here.
    """
    self.start_timer() # Starts calling handle_timer() at correct rate
    # Let's contain key, value pair of dst : (port, latency)
    # Need to differentiate host vs switch b/c cannot delete links to hosts?
    # port -> tells where to put the packet to find someone
    # So the routepacket will list the dst on the other side of the port as the next hop!
    """
    Table =     Host/port   s1   s2   s3
            h1      (latency/exp_time)...

            h2

            h3
    """


    self.routetbl = {} # host :  port : [latency, exp_time]
    self.links = {} # Handles ports to latency

  def handle_link_up (self, port, latency):
    """
    Called by the framework when a link attached to this Entity goes up.

    The port attached to the link and the link latency are passed in.
    """
    # WE GET THE LATENCY OF THE LINK!!! SO WE CAN GIVE OUR TBLS AND WE CAN UPDATE OUR TABLE?
    # Also, we need to send out updated information about our local hosts
    self.links[port] = latency #Make an entry to include this port to latency
    for destination in self.routetbl: #For all my accessible hosts
      if not self.routetbl[destination]:
        pass
      else:
        self.send(basics.RoutePacket(destination, self.routetbl[destination][self.find_best_port(destination)][0]), port) #Send a routepacket with destination, hostX, with latency + your listed latency

# Helper function to find best port to get to host for advertising
  def find_best_port (self, host):
    # Given a host, find the best port with latency in the dictionary
    best_port = None
    for aport in self.routetbl[host]:
      if best_port is None:
        best_port = aport
      else:
        if self.routetbl[host][best_port][0] > self.routetbl[host][aport][0]:
          best_port = aport
    return best_port

  def handle_link_down (self, port):
    """
    Called by the framework when a link attached to this Entity does down.

    The port number used by the link is passed in.
    """
    if self.POISON_MODE:
      for dst in self.routetbl:
        if port in self.routetbl[dst]:
          self.routetbl[dst][port] = [INFINITY, api.current_time() + 15]
          if self.find_best_port(dst) is port:
            self.update(dst, port)          
      self.links[port] = INFINITY
    else:
      for dst in self.routetbl:
        if port in self.routetbl[dst]:
          self.routetbl[dst].pop(port)
      self.links.pop(port)


  def handle_rx (self, packet, port):
    """
    Called by the framework when this Entity receives a packet.

    packet is a Packet (or subclass).
    port is the port number it arrived on.

    You definitely want to fill this in.
    """
    #self.log("RX %s on %s (%s)", packet, port, api.current_time())
    # Can get three packets: routepacket, HostsDiscoveryPacket, and normal packet
    # self.print_routingtable()
    if isinstance(packet, basics.RoutePacket):
      # Update my table with new packet information if known or unknown
      # Known destinations will be updated with the information of route packets
      self.handle_rp(packet, port)
    elif isinstance(packet, basics.HostDiscoveryPacket):
      # Put in routing table entry's for a hop's way to dst the information of latency and expiration time
      self.handle_hdp(packet, port)
    else:
      if packet.dst not in self.routetbl:
        return
      # print("Sending packet through " + str(self.find_best_port(packet.dst)))
      self.send(packet, self.find_best_port(packet.dst))

  def handle_rp(self, packet, port):
    """
    Checklist: I want to calculate new latency through the routing packet entry
    add routing packet entry to my table according to destination
    Update my neighbors of this new entry if this is the best routing entry
    Packet's latency can be infinity so handle that 

    If packet.destination is not in my routetbl, then I can safely add it because I definitely don't have it
    After adding the entry, I must tell my neighbors about this route
    If it is, then the destination I have heard, but could have best or not
      best => I can compare latencies
        lower latency due to same path or different path => better_route => tell my neighbors
        higher_latency for my only path => still best path => tell my neighbors
      not => I have no latencies to compare to
        best path => tell everyone but person who told me
    """
    new_latency = packet.latency + self.links[port]
    if packet.latency == INFINITY:
      new_latency = INFINITY
    if packet.destination not in self.routetbl or not self.routetbl[packet.destination]:
      self.routetbl[packet.destination] = {port:[new_latency, api.current_time() + 15]}
      self.update(packet.destination, port)
    else:
        best_latency = self.routetbl[packet.destination][self.find_best_port(packet.destination)][0]
        self.routetbl[packet.destination][port] = [new_latency, api.current_time() + 15]
        # print("Comparing two latencies: " + str(new_latency) + ", old: " + str(best_latency))
        if best_latency is not self.routetbl[packet.destination][self.find_best_port(packet.destination)][0]:
          self.update(packet.destination, port)

  def handle_hdp(self, packet, port):
    """
    CHECKLIST: Add the entry to my routing table
    The host is the dst, the latency is the port's latency
    """
    if packet.src not in self.routetbl or not self.routetbl[packet.src]:
      self.routetbl[packet.src] = {port : [self.links[port], None]}
      self.update(packet.src, port)
    else:
        best_latency = self.routetbl[packet.src][self.find_best_port(packet.src)][0]
        self.routetbl[packet.src][port] = [self.links[port], None]
        if best_latency is not self.routetbl[packet.src][self.find_best_port(packet.src)][0]:
          self.update(packet.src, port)

  def update(self, destination, port):
    # Called when the table was actually updated with one path to host becoming better
    if self.POISON_MODE:
      for link in self.links:
        if self.routetbl[destination][port][0] == INFINITY:
          # Poison Reverse? + Telling neighbors you got a poison path to some host
          if port is not link:
            self.send(basics.RoutePacket(destination, INFINITY), link)
        else:
          self.send(basics.RoutePacket(destination, self.routetbl[destination][port][0]), link)
    else:
      # Split Horizon bois!
      for link in self.links:
        # Don't advertise back to best port
        if port is not link:
          self.send(basics.RoutePacket(destination, self.routetbl[destination][port][0]), link)

  def handle_update(self):
    # SHOULD ONLY BE CALLED FOR HANDLE TIMER BECAUSE HANDLE TIMER SENDS OUT TABLE
    """
    Handle update will be called whenever handle_timer is called
    So handle update should tell my neighbors my current routing table
    So for every link I will send a routing packet saying that a non-expired path that is not through the link is my best path to that person
    """
    for link in self.links:
      for destination in self.routetbl:
        if not self.routetbl[destination]:
          pass
        elif self.find_best_port(destination) is not link:
          if self.POISON_MODE and self.routetbl[destination][self.find_best_port(destination)][0] == INFINITY:
              self.send(basics.RoutePacket(destination, INFINITY), link)
          else:
            self.send(basics.RoutePacket(destination, self.routetbl[destination][self.find_best_port(destination)][0]), link)
        elif self.routetbl[destination][self.find_best_port(destination)][0] == INFINITY and self.POISON_MODE:
          self.send(basics.RoutePacket(destination, INFINITY), link)

  def handle_timer (self):
    """
    Called periodically.

    When called, your router should send tables to neighbors.  It also might
    not be a bad place to check for whether any entries have expired.
    """
    for host in self.routetbl:
      toIter = list(self.routetbl[host])
      for port in toIter:
        if self.routetbl[host][port][1] is None:
          pass
        elif self.routetbl[host][port][1] <= api.current_time():
          self.routetbl[host].pop(port)
    self.handle_update()

  def print_routingtable(self):
    print("Host, port: lat,exptime of " + str(self))
    for host in self.routetbl:
      for port in self.routetbl[host]:
        print(str(host) + ", " + str(port) + ": [" + str(self.routetbl[host][port][0]) + ", " + str(self.routetbl[host][port][1]) + "]")
